<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ReceipeController;

use App\Receipe;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ReceipeController::class, 'index'])->name('rhome');

Route::get('/receipe/{id}', [ReceipeController::class, 'show'])->name('rshow');


Route::get('/create', [ReceipeController::class, 'create'])->name('rcreate');
Route::post('/create', [ReceipeController::class, 'create']);


Route::post('/create/save', [ReceipeController::class, 'create_save'])->name('rcreate_save');



Route::get('/api/receipe/{id}', function (){

    dd( (string) Receipe::find(17)->makeHidden('id') );

});