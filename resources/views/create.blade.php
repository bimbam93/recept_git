<!-- welcome.blade.php -->

@extends('layouts.basic')


@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <h2>Létrehozás</h2>

            <form action="{{route('rcreate_save')}}" method="POST">
                @csrf
                <input type="hidden" name="ing_num" value="{{ $ing_num }}">

                <label class="form-label"> Hozzávalók: {{ $ing_num }} </label>
                
                <div class="mb-2">    
                    <label class="form-label"> Megnevezés: </label>
                    <input class="form-control" type="text" name="name" value="{{ $name }}"/>                
                    @if ($errors->has('name'))
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                    @endif
                </div>

                <div class="mb-2">
                    <label class="form-label"> Elkészítési idő </label>
                    <input class="form-control" type="text" name="time"/>
                    @if ($errors->has('time'))
                        <small class="text-danger">{{ $errors->first('time') }}</small>
                    @endif
                </div>
                
                <div class="mb-2">
                    <label class="form-label"> Nehézség </label>
                    <input class="form-control" type="number" name="difficulty"/>
                    @if ($errors->has('difficulty'))
                        <small class="text-danger">{{ $errors->first('difficulty') }}</small>
                    @endif
                </div>

                <div class="mb-2">
                    <label class="form-label"> Értékelés </label>
                    <input class="form-control" type="number" name="score"/>
                    @if ($errors->has('score'))
                        <small class="text-danger">{{ $errors->first('score') }}</small>
                    @endif
                </div>

                <div class="mb-2">
                    <label class="form-label"> Leírás </label>
                    <textarea class="form-control" name="description"></textarea>
                    @if ($errors->has('description'))
                        <small class="text-danger">{{ $errors->first('description') }}</small>
                    @endif
                </div>

                <h3> Hozzávalók </h3>

                @for($i = 0; $i < $ing_num; $i++)
                    Hozzávaló - {{ $i+1 }}.
                    <div class="mb-2">    
                        <label class="form-label"> Megnevezés: </label>
                        <input class="form-control" type="text" name="ing[{{ $i }}][name]"/>                
                        @if ($errors->has('ing.'.$i.'.name'))
                            <small class="text-danger">{{ $errors->first('ing.'.$i.'.name') }}</small>
                        @endif
                    </div>
                    <div class="mb-2">    
                        <label class="form-label"> Hűtőben tartandó: </label>
                        <input class="form-control" type="text" name="ing[{{ $i }}][fridge]"/>                
                        @if ($errors->has('ing.'.$i.'.fridge'))
                            <small class="text-danger">{{ $errors->first('ing.' . $i . '.fridge') }}</small>
                        @endif
                    </div>
                    <div class="mb-2">    
                        <label class="form-label"> Mértékegység: </label>
                        <input class="form-control" type="text" name="ing[{{ $i }}][mou]"/>                
                        @if ($errors->has('ing.'.$i.'.mou'))
                            <small class="text-danger">{{ $errors->first('ing.'.$i.'.mou') }}</small>
                        @endif
                    </div>
                    <div class="mb-2">    
                        <label class="form-label"> Mennyiség: </label>
                        <input class="form-control" type="text" name="ing[{{ $i }}][unit]"/>                
                        @if ($errors->has('ing.'.$i.'.unit'))
                            <small class="text-danger">{{ $errors->first('ing.'.$i.'.unit') }}</small>
                        @endif
                    </div>
                @endfor


                <input class="btn btn-primary" type="submit" value="Létrehozás">
            </form>
        </div>
        <div class="col-sm-4" style="border-left: solid  rgb(185, 179, 179) 1px;">
            <h2>Receptek</h2>

            <div class="list-group">            
                @foreach ($receipes as $r)
                    <a href="{{ route('rshow', ['id'=>$r->id]) }}" class="list-group-item list-group-item-action">
                        {{ $r->name }} -                         
                        @for($i = 0; $i < $r->score; $i++)
                            *
                        @endfor
                    </a>
                @endforeach
            </div>    
                
        </div>
    </div>
</div>

@endsection