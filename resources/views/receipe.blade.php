<!-- receipe.blade.php -->

@extends('layouts.basic')


@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12">
               <h1>Recept - {{ $receipe->name }}</h1>
                <small>{{ $receipe->month }}. hónapban létrehozva</small>

                <table class="table">
                    <tr>
                        <th>
                            Paraméter
                        </th>
                        <td>
                            Értéke
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Név:
                        </th>
                        <td>
                            {{ $receipe->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Elékészítési idő:
                        </th>
                        <td>
                            {{ $receipe->time }}
                        </td>
                    </tr>
                </table>

                Hozzávalók:
                <ul>
                @foreach ($receipe->ingredients as $ing)

                    <li> {{ $ing->name }} ( {{ $ing->unit }} {{ $ing->mou }} ) </li>

                @endforeach
                <ul>
        </div>
    </div>
</div>

@endsection