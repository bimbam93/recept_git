<!-- welcome.blade.php -->

@extends('layouts.basic')


@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <h2>Létrehozás</h2>
            
            <form action="{{ route('rcreate') }}" method="POST">
                @csrf

                <div class="mb-2">
                    <label class="form-label"> Megnevezés </label>
                    <input class="form-control" type="text" name="name"/>
                </div>
                <div class="mb-2">
                    <label class="form-label">Hozzávalók száma </label>
                    <input class="form-control" type="number" name="ing_num"/>
                </div>
                <input class="btn btn-primary" type="submit" value="Létrehozás">
            </form>
        </div>
        <div class="col-sm-4"  style="border-left: solid rgb(185, 179, 179) 1px;">
            <h2>Receptek</h2>

            <div class="list-group">            
                @foreach ($receipes as $r)
                    <a href="{{ route('rshow', ['id'=>$r->id]) }}" class="list-group-item list-group-item-action">
                        {{ $r->name }} -                         
                        @for($i = 0; $i < $r->score; $i++)
                            *
                        @endfor
                    </a>
                @endforeach
            </div>    
                
        </div>
    </div>
</div>

@endsection