<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use App\Ingredient;

class Receipe extends Model
{

    use SoftDeletes;    

    //receipes
    ///protected $table = 'receipes';

    //id
    //protected $primaryKey = 'id';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];



    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];


    protected $appends = ['month'];


    public function ingredients(){
        return $this->hasMany(Ingredient::class);
    }



    public function getMonthAttribute(){

        return Carbon::parse($this->created_at)->month;

    }

    

}

