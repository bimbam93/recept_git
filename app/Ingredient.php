<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Receipe;

class Ingredient extends Model
{
    

    /**
     * Get the post that owns the comment.
     */    
    public function receipe()
    {
        return $this->belongsTo(Receipe::class);
    }
    
}
