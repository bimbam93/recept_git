<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReceipePostRequest;
use App\Receipe;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ReceipeController extends Controller
{
    
    public function index(){

        $receipes = Receipe::all();

        return view('welcome', [
                'receipes' => $receipes,
            ]);
    }

    public function show($id){

        $receipe = Receipe::find($id);

        if($receipe == null)
        {
            return redirect()->route('rhome');
        }

        return view('receipe', [
            'receipe' => $receipe,
        ]);
    }

    public function create(Request $req){

        $receipes = Receipe::all();
        
        $r_name = $req->input('name');
        if($req->input('name') == null){
            $r_name = "Recept - ?";
        }
        
        $r_ing_num = $req->input('ing_num');
        if($req->input('ing_num') == null){
            $r_ing_num = 2;
        }

        return view('create',[
            'name' => $r_name,
            'ing_num' => $r_ing_num,
            'receipes' => $receipes,
        ]);
    }

    //TODO
    public function create_save(ReceipePostRequest $req){

        $params = $req->only(['name', 'time', 'difficulty', 'description', 'score']);

        $now = Carbon::now();

        $params['created_at'] = $now->toDateTimeString();
        $params['updated_at'] = $now->toDateTimeString();
        $params['id'] = null;
        $params['deleted_at'] = null;

        $p = $params;
/*
        DB::insert('insert into receipes 
        (id, name, time, difficulty, description, score, created_at, updated_at, deleted_at) 
        values 
        (null, ?, ?, ?, ?, ?, ?, ?, ?)', 
        [
            $p['name'], $p['time'], $p['difficulty'], $p['description'], $p['score'], "2020-10-20 20:20:20", "2020-10-20 20:20:20", null
        ]);
*/

        $rid = DB::table('receipes')->insertGetId(
            $params
        );

        foreach($req->input('ing') as $ing) {
            
            $params = $ing;

            $params['receipe_id'] = $rid;
            $params['created_at'] = $now->toDateTimeString();
            $params['updated_at'] = $now->toDateTimeString();

            //dd($params);

            DB::table('ingredients')->insert(
                $params
            );

        }

        dd($req->all());
    
    }

}
