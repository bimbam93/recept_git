<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReceipePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
    
        return true;
    
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ing_num' => 'required|integer|between:1,3',
            'name' => 'required|string',
            'time' => 'required|string',
            'difficulty' => 'required|integer|between:0,5',
            'score' => 'required|integer|between:0,5',
            'description' => 'required',

            'ing.*.name' => 'required|string',
            'ing.*.fridge' => 'required|boolean',
            'ing.*.mou' => 'required|string',
            'ing.*.unit' => 'required|integer',            
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'A(z) :attribute mezőt közelező megadni!',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        $attr = [
            'ing_num' => 'Hozzávalók száma',
            'name' => 'Recept neve',
            'time' => 'Elkészítési idő',
            'difficulty' => 'Nehézség',
            'score' => 'Értékelés',
            'description' => 'Leírás',
        ];

        for($i = 0; $i<3; $i++){
            $attr['ing.'.$i.'.name'] = 'Hozzávaló neve';
            $attr['ing.'.$i.'.fridge'] = 'Hütőben tartandó';
            $attr['ing.'.$i.'.mou'] = 'Mértékegység';
            $attr['ing.'.$i.'.unit'] = 'Mennyiség';
        }

        return $attr;
    }
}
